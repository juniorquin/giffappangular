import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Gif, SearchGifsResponse } from '../interface/gifs.interface';

@Injectable({
  providedIn: 'root'
})
export class GifsService {

  private apiKey: string = 'eGHfZ5Ilg6EYsr5F53QpmWUzZQaea9VP'
  private baseUrl: string = 'https://api.giphy.com/v1/gifs'
  private _historial:string[] = []

  public resultados: Gif[] = []

  get historial():string[]{
    return [...this._historial]
  }

  constructor(
    private http: HttpClient
  ){
    this._historial = JSON.parse(localStorage.getItem('historial')!) || []

    this.resultados = JSON.parse(localStorage.getItem('resultados')!) || []
  }

  buscarGifs(query:string):void{

    query = query.trim().toLowerCase()

    if(this._historial.indexOf(query)===-1){
      this._historial.unshift(query)
      this._historial= this._historial.splice(0,10)
      localStorage.setItem('historial',JSON.stringify(this._historial))
    }else{
      const index = this._historial.indexOf(query)
      const arregloPorEncimaDeValor = this._historial.slice(0,index)
      arregloPorEncimaDeValor.unshift(query)
      this._historial.splice(0,index+1)
      this._historial = arregloPorEncimaDeValor.concat(this._historial)

    }

    const params = new HttpParams()
                    .set('api_key',this.apiKey)
                    .set('limit','10')
                    .set('q',query);

    this.http.get<SearchGifsResponse>(`${this.baseUrl}/search`,{params})
      .subscribe((response)=>{
        this.resultados = response.data
        localStorage.setItem('resultados',JSON.stringify(this.resultados))
      })
  }

  registrarLP():void{


    const payload = new HttpParams()
        .set('pcLenAbreviatura','PA')
        .set('pcLenNombre','PRUEBA ANGULAR 50')
        .set('pnLenId','0')
        .set('pcOpcion','01')
        .set('pcLenEliminado','')
        .set('DBConexion','SARPERBDConexion')


    this.http.post<any>('http://wariwdt.sesitdigital.com:8080/SARAPIDesarrollo/api/LenguajeProgramacion/MNT_LenguajeProgramacion',payload)
      .subscribe(res=>{
        console.log(res)
      })

  }
}
